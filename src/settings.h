/*
 * SLANG - Test (Test.cpp)
 * Copyright (C) 2017 Lukas | @Sakul6499 [Sakul6499@live.de]
 * Created on 29.05.2017.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * SLANG Settings class.
 * Stores all settings (globally).
 */
#ifndef SLANG_SETTINGS_H
#define SLANG_SETTINGS_H

#include "defs.h"
#include "model_interpreter_modes.hpp"

/**
 * Default SLANG namespace
 */
namespace slang {
    /**
     * Settings namespace
     * [Probably abuse of C-namespacing]
     */
    namespace settings {
        /* Required fields */
        /**
                 * The input stream.
                 * Will be created by the main function.
                 */
        FileInputStream input;
        /**
                 * The output stream.
                 * Will be created by the main function.
                 */
        FileOutputStream output;
        /**
                 * The working mode.
                 */
        model::modes::Modes mode;
        /* Optional fields */
        /**
                 * if enabled, the literal AST output will be printed to console (std::cout).
                 */
        bool printLiteralAST = false;
        /**
                 * if set, the literal AST output will be stored into stream.
                 */
        FileOutputStream literalASTOutput;
        /**
                 * if enabled, the encoded AST output will be printed to console (std::cout).
                 */
        bool printEncodedAST = false;
        /**
                 * if set, the encoded AST output will be stored into stream.
                 */
        FileOutputStream encodedASTOutput;
        /**
                 * if enabled, the generated cpp code will be printed to console (std::cout).
                 * Note: Only applies, if cpp source will be generated.
                 */
        bool printCppSource = false;
        /**
                 * if set, the generated cpp source will be stored into stream.
                 * Note: Only applies, if cpp source will be generated.
                 */
        FileOutputStream cppSourceOutput;
    }
}
#endif //SLANG_SETTINGS_H
