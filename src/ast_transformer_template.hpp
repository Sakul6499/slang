//
// Created by Lukas on 02.06.2017.
//

#ifndef SLANG_AST_TRANSFORMER_TEMPLATE_HPP
#define SLANG_AST_TRANSFORMER_TEMPLATE_HPP

#include "defs.h"
#include "model_ast.hpp"

namespace slang {
    namespace ast_transformer {
        class ast_transformer_template {
        public:
            ast_transformer_template(Vector<ASTObject> source) : source(source) {}

            virtual ~ast_transformer_template() {
                source.clear();
            }

            virtual String transform() = 0;

        protected:
            Vector<ASTObject> source;
        };
    }
}

#endif //SLANG_AST_TRANSFORMER_TEMPLATE_HPP
