/*
 * SLANG - Test (Test.cpp)
 * Copyright (C) 2017 Lukas | @Sakul6499 [Sakul6499@live.de]
 * Created on 29.05.2017.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * SLANG Util class
 * Contains some util functions
 */
#ifndef SLANG_UTILS_H
#define SLANG_UTILS_H

// Default Includes
#include "defs.h"

// mkdir + stats
#include <sys/stat.h>

/**
 * Default SLANG namespace
 */
namespace slang {
    /**
     * Util namespace
     */
    namespace util {
        /**
         * Get's the path (excludes filename and extension).
         *
         * INPUT                ->  OUTPUT
         * "file.ext"           ->  ""
         * "/folder/"           ->  "/folder/"
         * "/folder/file.bin"   ->  "/folder/"
         *
         * @param full_path the full path of the folder or file
         * @return the path without filename and extension
         */
        String getPath(String full_path) {
            // eg. "file.ext"
            if (full_path.find("/") == String::npos)
                return "";

            // eg "/folder/"
            if (full_path.back() == '/')
                return full_path;

            // eg. "/folder/file.ext"
            return full_path.substr(0, full_path.find_last_of("/"));
        }

        String getFile(String full_path) {
            String::size_type pos = full_path.find("/");
            if(pos == String::npos) return full_path;
            return full_path.substr(pos + 1, full_path.length());
        }

        /**
         * Creates an directory (path).
         * Like 'mkdir' but creates an entire path.
         *
         * Note: Makes use of "mkdir -p <path>" sys-cmd!
         *
         * @param path the path, which shall be created.
         * @returns the (final) mkdir result
         */
        int mkpath(String path) {
            String::size_type pre = 0, pos;
            String dir;
            int mdret;

            // Force a '/' at the end
            if (path[path.size() - 1] != '/') {
                path += '/';
            }

            while ((pos = path.find_first_of('/', pre)) != String::npos) {
                dir = path.substr(0, pos++);
                pre = pos;

                // Skip leading '/'
                if (dir.size() == 0) continue;

#ifdef __linux__
                if ((mdret = mkdir(dir.c_str(), 777)) && errno != EEXIST) {
                    return mdret;
                }
#else
                if ((mdret = mkdir(dir.c_str())) && errno != EEXIST) {
                    return mdret;
                }
#endif
            }
            return -127;
        }

        void rmdir(String path) {
#ifdef __linux__
            system(("rm -rf " + path).c_str());
#else
            system(("DEL /Q /F /S " + path).c_str());
#endif
        }
    }
}
#endif //SLANG_UTILS_H
