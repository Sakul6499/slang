//
// Created by Lukas on 02.06.2017.
//

#include "ast_transformer_target_cpp.hpp"

slang::ast_transformer::ast_transformer_target_cpp::ast_transformer_target_cpp(
        const Vector<slang::model::ast::ASTObject> &source) : ast_transformer_template(source) {}

String slang::ast_transformer::ast_transformer_target_cpp::transform() {
    debug(source.size());
    String result = "";

    /* pre transform */

    int curly_bracket_counter = 0;
    Vector<int> entrys = Vector<int>();

    for (int i = 0; i < source.size(); i++) {
        ASTObject &elem = source[i];

        if (elem.type == ASTType::CHAR_CURLY_BRACKET_OPEN) {
            // first check
            if (curly_bracket_counter == 0) entrys.push_back(i);
            // then add
            curly_bracket_counter++;
        } else if (elem.type == ASTType::CHAR_CURLY_BRACKET_CLOSE) {
            // first subtract
            curly_bracket_counter--;
            // then check if zero
            if (curly_bracket_counter == 0) entrys.push_back(i);
        }

        if (curly_bracket_counter < 0) {
            println("Error: curly bracket counter dropped below zero within cpp [pre] transformer")
            println("Object #" << i << " " << elem);
            exit(-1);
        }
    }

    if (curly_bracket_counter > 0) {
        println("Error: curly bracket counter ended above zero within cpp [pre] transformer")
        exit(-1);
    }

    debug(entrys.size());
    for (int a = 0; a < entrys.size(); a++) {
        int &e = entrys[a];
        println("#" << e << " " << source[e])

        if(source[e].type != ASTType::CHAR_CURLY_BRACKET_OPEN) {
            println("Warning: found non " << ASTType::CHAR_CURLY_BRACKET_OPEN << " object! Skipping ... [" << source[e] << "]");
            continue;
        }

        int entry_begin = -1;
        for (int i = e; i >= 0; i--) {
            ASTObject elem = source[i];

            switch (elem.type) {
                case ASTType::INDICATOR_CLASS: {
                    entry_begin = i;
                    break;
                }
                case ASTType::INDICATOR_FUNCTION: {
                    entry_begin = i;
                    break;
                }
                default: {
                    continue;
                }
            }
        }

        if (entry_begin < 0) {
            println("Error: did not found indicator in pre c++ transformer")
            exit(-1);
        }

        /* transform */
        int &entry_end = entrys[++a];
        ASTObject &ending = source[entry_end];
        if(ending.type != ASTType::CHAR_CURLY_BRACKET_CLOSE) {
            println("Error: ending in ast transformer [target c++] is not " << ASTType::CHAR_CURLY_BRACKET_CLOSE << "!");
            exit(-1);
        }

        int last_entry_ending = 0;

        ASTObject elem = source[entry_begin];
        debug(elem);
        if (elem.type == ASTType::INDICATOR_FUNCTION) {
            // TODO

            debug(entry_begin);
            debug(entry_end);
            debug(ending);

            // check if before function defintion is something
            if(last_entry_ending + 1 < entry_begin) {
                for(int f = last_entry_ending; f < entry_begin; f++) {
                    ASTObject &_elem = source[f];
                    if(_elem.type == ASTType::SOURCE) {
                        result += _elem.value + "\n";
                    }
                }
            }

            int entry_index = entry_begin;
            last_entry_ending = entry_end;

            // fun <name>
            ASTObject &name = source[++entry_index];
            if(name.type != ASTType::LABEL) {
                println("Error: Expected " << ASTType::LABEL << " after function indicator! [" << entry_index << "]")
                exit(-1);
            }

            // fun <name>(
            if(source[++entry_index].type != ASTType::CHAR_ROUND_BRACKET_OPEN) {
                println("Error: Expected " << ASTType::CHAR_ROUND_BRACKET_OPEN << " after function name! [" << entry_index << "]");
                exit(-1);
            }

            String arguments = "";
            // fun <name>(...)
            if(source[++entry_index].type != ASTType::CHAR_ROUND_BRACKET_CLOSE) {
                // TODO: args
                println("Warning: argument transformation currently not implemented! [" << source[entry_index] << "]");
            }

            // fun <name>(...) : <returnType>
            ASTObject returnType;
            if(source[++entry_index].type == ASTType::CHAR_COLON) {
                returnType = source[++entry_index];
            } else {
                returnType = {ASTType::LABEL, "void"};
            }

            // fun <name>(...) : <returnType> {
            if(source[++entry_index].type != ASTType::CHAR_CURLY_BRACKET_OPEN) {
                println("Error: Expected " << ASTType::CHAR_CURLY_BRACKET_OPEN << " after function name! [" << entry_index << "]");
                exit(-1);
            }

            /*
             * SLANG:
             * 1a. fun <name>() { }
             * 1b. fun <name>() { ... }
             * 2a. fun <name>() : <returnType> { }
             * 2b. fun <name>() : <returnType> { ... }
             * 3a. fun <name>(<type> <name>) { }
             * 3b. fun <name>(<type> <name>) { ... }
             * 3c. fun <name>(<type> <name>) : <returnType> { }
             * 3d. fun <name>(<type> <name>) : <returnType> { ... }
             * 4a. fun <name>(<name>: <type>) { }
             * 4b. fun <name>(<name>: <type>) { ... }
             * 4c. fun <name>(<name>: <type>) : <returnType> { }
             * 4d. fun <name>(<name>: <type>) : <returnType> { ... }
             *
             * C++:
             * 1a. void <name>() { }
             * 1b. void <name>() { ... }
             * 2a. void <returnType> <name>() { }
             * 2b. void <returnType> <name>() { ... }
             * 3a. -> 2a.
             * 3b. -> 2b.
             * 3c. <returnType> <name>(<type> <name>) { }
             * 3d. <returnType> <name>(<type> <name>) { ... }
             * 4a. -> 3a.
             * 4b. -> 3b.
             * 4c. -> 3c.
             * 4d. -> 3d.
             */

            // <returnType>
            result += returnType.value + " ";

            // <returnType> <name>(
            result += name.value + "(";

            if(name.value == "main") {
                result += "int argc, char* argv[]";
            }

            // <returnType> <name>(...) {
            result += arguments + ") {\n";

            for(int entries = entry_index; entries < entry_end; entries++) {
                ASTObject &bodyElement = source[entries];
                // TODO: body declaration
                result += bodyElement.value;
            }

            // END
            result += "\n}\n";

            for(int entries = entry_begin; entries < entry_end; entries++) {
                println("#" << entries << " " << source[entries]);
            }
        } else if (elem.type == ASTType::INDICATOR_CLASS) {
            // TODO
        } else {
            println("Error: Got unknown object " << elem << " while only accepting function or class indicators within c++ transformer!");
        }
    }

    /* transform */
//    for (auto const &elem : source) {
//        switch (elem.type) {
//            case model::ast::UNDEFINED: {
//                println("Error: Undefined ast object found in transformer!");
//                break;
//            };
//            case model::ast::SINGLE_LINE_COMMENT: {
//                println("Transform not implemented for element: " << elem);
//                break;
//            };
//            case model::ast::MULTI_LINE_COMMENT: {
//                println("Transform not implemented for element: " << elem);
//                break;
//            };
//            case model::ast::STRING: {
//                println("Transform not implemented for element: " << elem);
//                break;
//            };
//            case model::ast::SOURCE: {
//                result += elem.value + "\n";
//                break;
//            };
//            case model::ast::CHAR_CURLY_BRACKET_OPEN: {
//                println("Transform not implemented for element: " << elem);
//                break;
//            };
//            case model::ast::CHAR_CURLY_BRACKET_CLOSE: {
//                println("Transform not implemented for element: " << elem);
//                break;
//            };
//            case model::ast::CHAR_ROUND_BRACKET_OPEN: {
//                println("Transform not implemented for element: " << elem);
//                break;
//            };
//            case model::ast::CHAR_ROUND_BRACKET_CLOSE: {
//                println("Transform not implemented for element: " << elem);
//                break;
//            };
//            case model::ast::CHAR_SEMICOLON: {
//                println("Transform not implemented for element: " << elem);
//                break;
//            };
//            case model::ast::CHAR_DOT: {
//                println("Transform not implemented for element: " << elem);
//                break;
//            };
//            case model::ast::CHAR_COMMA: {
//                println("Transform not implemented for element: " << elem);
//                break;
//            };
//            case model::ast::CHAR_COLON: {
//                println("Transform not implemented for element: " << elem);
//                break;
//            };
//            case model::ast::CHAR_ADDITION: {
//                println("Transform not implemented for element: " << elem);
//                break;
//            };
//            case model::ast::CHAR_SUBSTRACTION: {
//                println("Transform not implemented for element: " << elem);
//                break;
//            };
//            case model::ast::CHAR_MULTIPLICATION: {
//                println("Transform not implemented for element: " << elem);
//                break;
//            };
//            case model::ast::CHAR_DIVISION: {
//                println("Transform not implemented for element: " << elem);
//                break;
//            };
//            case model::ast::INDICATOR_FUNCTION: {
//                println("Transform not implemented for element: " << elem);
//                break;
//            };
//            case model::ast::INDICATOR_CLASS: {
//                println("Transform not implemented for element: " << elem);
//                break;
//            };
//            case model::ast::INDICATOR_VARIABLE: {
//                println("Transform not implemented for element: " << elem);
//                break;
//            };
//            case model::ast::LABEL: {
//                println("Transform not implemented for element: " << elem);
//                break;
//            };
//        }
//    }

    return result;
}
