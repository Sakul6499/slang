/*
 * SLANG - Test (Test.cpp)
 * Copyright (C) 2017 Lukas | @Sakul6499 [Sakul6499@live.de]
 * Created on 29.05.2017.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Default Includes
#include "defs.h"
// SLANG Interpreter
#include "interpreter.cpp"
// Utils
#include "utils.h"
// Transformer - Target: C++
#include "ast_transformer_target_cpp.cpp"

/**
 * The header (most likely the application name).
 * Will be printed as soon as the application starts [> main entry].
 */
#define HEADER "SLANG"
/**
 * The author's name.
 * Will be printed after the HEADER.
 */
#define AUTHOR "Lukas | @Sakul6499"
/**
 * The current version.
 * Will be printed with the HEADER
 */
#define VERSION "0.2"

/**
 * Environment Variables
 */
extern char **environ;

/**
 * Will print the header to screen
 */
void printHeader() {
    println(HEADER << " v" << VERSION);
    println("by " << AUTHOR);
    emptyln;
}

/**
 * Main entry
 *
 * @param argc argument count
 * @param argv argument vector
 * @return exit code of application
 */
int main(int argc, char **argv) {
    try {
        // print header message
        printHeader();

        // define file names
        String inputFile = "";
        String outputFile = "";
        String literalASTFile = "";
        String encodedASTFile = "";
        String cppFile = "";

        // handle arguments
        // Note: Skip first entry (invocation command)
        for (int i = 1; i < argc; i++) {
            String arg = argv[i];
            if (arg == "-h" || arg == "--help" || arg == "-?") {
                println("# Common arguments")
                println("[-i <file>]\t--input <file>\tset the input file")
                println("[-o <file>]\t--output <file>\tset the output file (or directory)")
                println("[-m <mode>]\t--mode <mode>\tset the operation mode")
                emptyln
                println("# Literal AST")
                println("--print-literal-ast\t\tprint the generated literal ast")
                println("--literal-ast-output <file>\tset an output file to store the literal ast")
                emptyln
                println("# Encoded AST")
                println("--print-encoded-ast\t\tprint the generated encoded ast")
                println("--encoded-ast-output <file>\tset an output file to store the encoded ast")
                emptyln
                println("# C++ Source (only if mode is selected)")
                println("--print-cpp-source\t\tprint the generated C++ source")
                println("--cpp-source-output <file>\tset an output file to store the C++ source")
                emptyln
                println("# Other")
                println("[-h]\t\t--help\t\tdisplay this screen")
                println("\t\t--print-env\tdisplay environment variables")
                emptyln

                return 0;
            } else if (arg == "-i" || arg == "--input") {
                // define input

                if (argc - i <= 0) {
                    println("You need to specify an input file!");
                    return -1;
                }

                inputFile = argv[++i];
            } else if (arg == "-o" || arg == "--output") {
                // define output

                if (argc - i <= 0) {
                    println("You need to specify an output file!");
                    return -1;
                }

                outputFile = argv[++i];
            } else if (arg == "-m" || arg == "--mode") {
                // define mode

                if (argc - i <= 0) {
                    println("You need to specify a mode!");
                    return -1;
                }

                String mode = argv[++i];
                if (mode == "literal-ast" || mode == "ast") {
                    slang::settings::mode = slang::model::modes::LITERAL_AST;

                } else if (mode == "encoded-ast" || mode == "east") {
                    slang::settings::mode = slang::model::modes::ENCODED_AST;

                } else if (mode == "source-cpp" || mode == "scpp" || mode == "cpp-source") {
                    slang::settings::mode = slang::model::modes::SOURCE_CPP;

                } else if (mode == "binary-cpp" || mode == "cpp") {
                    slang::settings::mode = slang::model::modes::BINARY_CPP;

                    println("Warning: C++ Binary format not yet available!");
                } else if (mode == "slang-binary" || mode == "slang") {
                    slang::settings::mode = slang::model::modes::BINARY_SLANG;

                    println("SLANG Binary format not yet available!");
                    exit(-1);
                } else {
                    println("Mode '" << mode << "' unknown!");
                    exit(-1);
                }
            } else if (arg == "--print-literal-ast") {
                // enable literal-ast output

                slang::settings::printLiteralAST = true;
            } else if (arg == "--literal-ast-output") {
                // define output file for literal-ast

                if (argc - i <= 0) {
                    println("You need to specify an literal output file!");
                    return -1;
                }

                literalASTFile = argv[++i];
            } else if (arg == "--print-encoded-ast") {
                // enable encoded-ast output

                slang::settings::printEncodedAST = true;
            } else if (arg == "--encoded-ast-output") {
                // define output file for encoded-ast

                if (argc - i <= 0) {
                    println("You need to specify an encoded output file!");
                    return -1;
                }

                encodedASTFile = argv[++i];
            } else if (arg == "--print-cpp-source") {
                // enable cpp source output

                slang::settings::printCppSource = true;
            } else if (arg == "--cpp-source-output") {
                // define output file for cpp source
                if (argc - i <= 0) {
                    println("You need to specify an cpp output file!");
                    return -1;
                }

                cppFile = argv[++i];
            } else if (arg == "--print-env") {
                // print environment variables

                println("# ENVIRONMENT:")
                emptyln;

                char **env;
                for (env = environ; *env != nullptr; env++) {
                    char *__env = *env;
                    println(__env);
                }

                emptyln;
            } else {
                // unknown

                println("Unknown argument: " << arg);
            }
        }

        bool configured = true;
        // check if input file is set
        if (inputFile.size() == 0) {
            configured = false;
            println("No input file set!");
        }

        // check if mode was set (not undefined)
        if (slang::settings::mode == slang::model::modes::UNDEFINED) {
            configured = false;
            println("No mode set!");
        }

        // check if both (inputFile + mode) are set
        if (!configured) {
            println("Not all needed settings are set!");
            return -1;
        }

        // in case no output file was set OR a path was given -> default file name
        if (outputFile.size() == 0 || outputFile == slang::util::getPath(outputFile)) {
            println("No output file is set or output file is a directory! Defaulting filename ...")
            String filename;
            switch (slang::settings::mode) {
                case slang::model::modes::LITERAL_AST:
                    filename = "out.ast";
                    break;
                case slang::model::modes::ENCODED_AST:
                    filename = "out.east";
                    break;
                case slang::model::modes::SOURCE_CPP:
                    filename = "out.cpp";
                    break;
                case slang::model::modes::BINARY_CPP:
                    filename = "out.cpp";
                    break;
                case slang::model::modes::BINARY_SLANG:
                    filename = "out.slang";
                    break;
                default:
                    println("No mode or no valid mode specified! Couldn't handle output file creation ...");
                    return -1;
            }

            if (outputFile.size() == 0) {
                outputFile = filename;
            } else {
                outputFile += filename;
            }
        }

        // eventually later used
        String outputFolder = slang::util::getPath(outputFile);

        // Clear directory
        slang::util::rmdir(outputFolder);

        // print and open input file
        println("Input File: " << inputFile);
        slang::settings::input = FileInputStream(inputFile, std::ios::in | std::ios::binary | std::ios::ate);
        if (!slang::settings::input.is_open()) {
            println("Couldn't open file input stream for '" << inputFile << "'!");
            return -1;
        }

        // print and open output file
        println("Output File: " << outputFile);
        slang::util::mkpath(slang::util::getPath(outputFile));
        slang::settings::output = FileOutputStream(outputFile, std::ios::out | std::ios::binary | std::ios::ate);
        if (!slang::settings::output.is_open()) {
            println("Couldn't open file output stream for '" << outputFile << "'!");
            return -1;
        }

        // [if literal ast file was set ->] print and open file
        if (literalASTFile.size() > 0) {
            println("Literal AST File: " << literalASTFile);
            slang::util::mkpath(slang::util::getPath(literalASTFile));
            slang::settings::literalASTOutput = FileOutputStream(literalASTFile,
                                                                 std::ios::out | std::ios::binary | std::ios::ate);
            if (!slang::settings::literalASTOutput.is_open()) {
                println("Couldn't open file output stream for '" << literalASTFile << "'!");
                return -1;
            }
        }

        // [if encoded ast file was set ->] print and open file
        if (encodedASTFile.size() > 0) {
            println("Encoded AST File: " << encodedASTFile);
            slang::util::mkpath(slang::util::getPath(encodedASTFile));
            slang::settings::encodedASTOutput = FileOutputStream(encodedASTFile,
                                                                 std::ios::out | std::ios::binary | std::ios::ate);
            if (!slang::settings::encodedASTOutput.is_open()) {
                println("Couldn't open file output stream for '" << encodedASTFile << "'!");
                return -1;
            }
        }

        // [if cpp file was set ->] print and open file
        if (cppFile.size() > 0) {
            println("C++ File: " << cppFile);
            slang::util::mkpath(slang::util::getPath(cppFile));
            slang::settings::cppSourceOutput = FileOutputStream(cppFile,
                                                                std::ios::out | std::ios::binary | std::ios::ate);
            if (!slang::settings::cppSourceOutput.is_open()) {
                println("Couldn't open file output stream for '" << cppFile << "'!");
                return -1;
            }
        }

        // clear strings up
        inputFile.clear();
        // Note: do not clear output file
        literalASTFile.clear();
        encodedASTFile.clear();
        cppFile.clear();

        // initialize slang interpreter
        SLANGInterpreter interpreter;
        interpreter.saveSourceFromStream(slang::settings::input, true);
        Vector<ASTObject> astVector = interpreter.parse();

        // print literal AST
        if (slang::settings::printLiteralAST) {
            emptyln
            println("# Printing literal AST")

            for (ASTObject astObject : astVector) {
                println(astObject);
            }
            emptyln
        }

        // save literal AST
        if (slang::settings::literalASTOutput.is_open()) {
            for (ASTObject astObject : astVector)
                slang::settings::literalASTOutput << astObject << "\n";

            slang::settings::literalASTOutput.close();
        }

        // print encoded AST
        if (slang::settings::printEncodedAST) {
            emptyln
            println("# Printing encoded AST")
            for (ASTObject astObject : astVector) {
                println(slang::model::ast::ASTObjectToString(astObject, true));
            }
            emptyln
        }

        // save encoded AST
        if (slang::settings::encodedASTOutput.is_open()) {
            for (ASTObject astObject : astVector)
                slang::settings::encodedASTOutput << slang::model::ast::ASTObjectToString(astObject, true) << "\n";

            slang::settings::encodedASTOutput.close();
        }

        switch (slang::settings::mode) {
            case slang::model::modes::UNDEFINED: {
                println("Error: Undefined mode!")
                return -1;
            }
            case slang::model::modes::LITERAL_AST: {
                for (ASTObject astObject : astVector)
                    slang::settings::output << astObject << "\n";

                slang::settings::output.close();
                break;
            }
            case slang::model::modes::ENCODED_AST: {
                for (ASTObject astObject : astVector)
                    slang::settings::output << slang::model::ast::ASTObjectToString(astObject, true) << "\n";

                slang::settings::output.close();
                break;
            }
            case slang::model::modes::SOURCE_CPP: {
                slang::ast_transformer::ast_transformer_target_cpp transformer(astVector);
                String transformerResult = transformer.transform();
                debug(transformerResult);

                slang::settings::output << transformerResult;
                slang::settings::output.close();

                // print cpp source
                if (slang::settings::printCppSource) {
                    emptyln
                    println("# Printing cpp source")
                    println(transformerResult);
                    emptyln
                }

                // save cpp source
                if (slang::settings::cppSourceOutput.is_open()) {
                    slang::settings::cppSourceOutput << transformerResult;
                    slang::settings::cppSourceOutput.close();
                }
                break;
            }
            case slang::model::modes::BINARY_CPP: {
                emptyln

                // check for requirements
//                        println("Using GCC: ");
//                if(system("gcc --version") != 0) {
//                    println("Warning: GCC didn't found!");
//                    exit(-1);
//                }
//                emptyln

                println("Using CMake: ");
                if(system("cmake --version") != 0) {
                    println("Warning: CMake didn't found!");
                    exit(-1);
                }

                        println("Transforming SLANG to C++ ...")
                slang::ast_transformer::ast_transformer_target_cpp transformer(astVector);
                String transformerResult = transformer.transform();
                debug(transformerResult);

                slang::settings::output << transformerResult;
                slang::settings::output.close();

                // print cpp source
                if (slang::settings::printCppSource) {
                    emptyln
                            println("# Printing cpp source")
                    println(transformerResult);
                    emptyln
                }

                // save cpp source
                if (slang::settings::cppSourceOutput.is_open()) {
                    slang::settings::cppSourceOutput << transformerResult;
                    slang::settings::cppSourceOutput.close();
                }

//                // compile
//                String cmd = "pushd " + outputFolder + " && gcc " + slang::util::getFile(outputFile) + " && popd";
//                debug(cmd)
//                if(system(cmd.c_str()) != 0) {
//                    println("Error: Compilation failed!");
//                    exit(-1);
//                }

                // CMakeFile
                println("Setting up CMakeLists ...")
                debug(outputFolder);
                String cmakeFileName = outputFolder + "/CMakeLists.txt";
                debug(cmakeFileName);
                FileOutputStream cmakeFileStream(cmakeFileName, std::ios::out | std::ios::binary | std::ios::ate);
                if (!cmakeFileStream.is_open()) {
                    println("Error: Unable to open " << cmakeFileName << "!");
                    exit(-1);
                }

                cmakeFileStream << "cmake_minimum_required(VERSION 3.5.1)" << "\n";
                cmakeFileStream << "project(out)" << "\n";
                cmakeFileStream << "set(CMAKE_CXX_STANDARD 14)" << "\n";
                cmakeFileStream << "set(CMAKE_EXE_LINKER_FLAGS \"-static-libgcc -static-libstdc++ -static\")" << "\n";
                cmakeFileStream << "set(SOURCE_FILES out.cpp)" << "\n";
                cmakeFileStream << "add_executable(out ${SOURCE_FILES})" << "\n";

                cmakeFileStream.close();

                // execute CMake
                println("Invoking CMake: ")
                if(system(("pushd " + outputFolder + " && cmake -G 'Unix Makefiles' . && popd").c_str()) != 0) {
                    println("Error: CMake failed");
                    exit(-1);
                }

                // build
                println("Invoking CMake build: ")
                if(system(("pushd " + outputFolder + " && cmake --build . --target out && popd").c_str()) != 0){
                    println("Error: CMake build failed!");
                    exit(-1);
                }

                break;
            }
            case slang::model::modes::BINARY_SLANG: {
                // TODO: Implement binary slang mode
                println("Error: Slang binary mode not yet implemented!")
                return -1;
            }
        }

        // exit
        println("Finished!");
        return 0;
    } catch (std::exception &e) {
        debug(e.what());
        println("An exception occurred during runtime!");
    } catch (...) {
        println("An unknown exception occurred during runtime!");
    }
}

// TODO: CLI option for 'compiling' comments
// TODO: CLI option for setting cmake, gcc etc. path [CC=x CXX=x]